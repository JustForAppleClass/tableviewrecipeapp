//
//  RecipeViewController.h
//  TableViewRecipe
//
//  Created by Matthew Griffin on 5/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RecipeViewController : UIViewController

@property(nonatomic, strong)
NSDictionary* RecipeHolder;

@end
