//
//  ViewController.h
//  TableViewRecipe
//
//  Created by Matthew Griffin on 5/23/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeViewController.h"


@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* RecipeArray;
}

@end

